import os
from typing import Any, Dict

import yaml
from yaml import representer

yaml.add_representer(
    dict, lambda self, data: representer.SafeRepresenter.represent_dict(self, data.items())
)


def load_yaml(path: str, safe: bool = False) -> Dict[str, Any]:
    if not os.path.isfile(path) and safe:
        return {}
    with open(path, "r") as cfg:
        yml = yaml.load(cfg, Loader=yaml.FullLoader)
        if not yml and safe:
            return {}
        return yml


def save_yaml(path: str, data: Dict[str, Any], default_flow_style: bool = False):
    with open(path, "w") as cfg:
        yaml.dump(data, cfg, default_flow_style=default_flow_style)
