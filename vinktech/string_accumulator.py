from typing import Any, List


class StringAccumulator:
    def __init__(self, separator: str = " "):
        self.separator: str = separator
        self.pieces: List[Any] = []

    def accumulate(self, piece: Any):
        self.pieces.append(piece)

    def get(self) -> str:
        val: str = self.separator.join(map(lambda s: StringAccumulator.__str(s), self.pieces))
        self.pieces.clear()
        return val

    def has_data(self) -> bool:
        return 0 < len(self.pieces)

    def __repr__(self) -> str:
        return self.separator.join(map(lambda s: StringAccumulator.__str(s), self.pieces))

    @staticmethod
    def __str(target: Any) -> str:
        if isinstance(target, str):
            return target
        return str(target)
