#!/usr/bin/env bash
filter="dist/zutun*.whl"

python setup.py bdist_wheel
pipenv lock -r > requirements.txt

output=$(ls ${filter})
count=$(ls ${filter} | wc -l)

if [[ ${count} -ne 1 ]]; then
  echo "expected to find only one target matching ${filter}; found ${count}: ${output}"
  exit 1
fi

for whl in ${output}; do
  px=$(echo ${whl} | sed -r "s/whl/pex/g")
  echo "Running pex ${whl} -r requirements.txt -o ${px} -v"
  pex ${whl} -r requirements.txt -o ${px} -v
  ret=$?
  if [[ $ret -ne 0 ]]; then
    exit $ret
  fi
  echo "Output file should be ${px}"
done