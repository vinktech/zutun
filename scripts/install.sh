#!/usr/bin/env bash

TYPE=${1:-"pip"}

DIR=${2:-"${HOME}/.zutun"}

mkdir -p ${DIR}/zutun_downloads

cd ${DIR}

rm -rf zutun_downloads/*

FROM="https://gitlab.com/vinktech/zutun/-/jobs/artifacts/master/download?job=build_pex"

wget ${FROM} -O zutun_downloads/latest.zip

(cd zutun_downloads && unzip -o latest.zip)

VERSION_FILE=zutun_version
NEW_VERSION=$(cat zutun_downloads/dist/${VERSION_FILE})
if [[ -f "${VERSION_FILE}" ]]; then
  EXISTING_VERSION=$(cat ${VERSION_FILE})
else
  EXISTING_VERSION=0
fi

if [[ "${NEW_VERSION}" != "${EXISTING_VERSION}" ]]; then
  echo "Download version ${NEW_VERSION} != installed version ${EXISTING_VERSION}, installing update"

  TYPE_FILE=zutun_type
  if [[ -f "${TYPE_FILE}" ]]; then
    EXISTING_TYPE=$(cat ${TYPE_FILE})
  else
    EXISTING_TYPE="${TYPE}"
  fi

  if [[ "${EXISTING_TYPE}" != "${TYPE}" ]]; then
    echo "Found existing install type of ${EXISTING_TYPE}, will use that"
    TYPE="${EXISTING_TYPE}"
  fi

  if [[ "pip" == "${TYPE}" ]]; then
    python3 -m pip install --user -r zutun_downloads/dist/requirements.txt
    python3 -m pip install --user zutun_downloads/dist/zutun-*.whl
    cp zutun_downloads/dist/${VERSION_FILE} ${VERSION_FILE}
  elif [[ "pipenv" == "${TYPE}" ]]; then
    python3 -m pipenv install -r requirements.txt
    python3 -m pipenv install zutun_downloads/dist/zutu-*.whl
    echo "Now copy the following bash script to an executable of your choice"
    echo ""
    echo "#!/usr/bin/env bash"
    echo "DIR=\"${DIR}\""
    echo "cd \${DIR}"
    echo "python3 -m pipenv run zutun \"\${@\}\""
  elif [[ "pex" == "${TYPE}" ]]; then
    cp zutun_downloads/dist/zutun-*.pex .
    echo "Now copy the following bash script to an executable of your choice"
    echo ""
    echo "#!/usr/bin/env bash"
    echo "DIR=\"${DIR}\""
    echo "cd \${DIR}"
    echo "python3 zutun-*.pex -m zutun.twcli.main \"\${@\}\""
  else
    echo "Unknown install type ${TYPE}"
  fi

else
  echo "Versions match, nothing to do"
fi
