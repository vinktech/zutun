# todo
#### 2020-07-20
* add levels based on age of task
* add support for comments on completion
* decide on how to store tasks - two files, todo/done?
* decide on command line parsing
* complete/abort?
* what about ids
* dateparser is your friend, but figure out how to extract from text
* tests pls
 
# done
#### 2020-07-20
* decide on how to store dates - see `zutun.dates`
