import enum

ANSI_ENDC: str = "\033[0m"


class Colour:
    def __init__(self, name: str, ansi: str, hex_: str) -> None:
        self.name: str = name
        self.ansi: str = ansi
        self.hex: str = hex_

    def wrap_cli(self, text: str) -> str:
        if "" != self.ansi:
            return f"{self.ansi}{text}{ANSI_ENDC}"
        return text


class Colours(enum.Enum):
    Grey = Colour("grey", "\033[90m", "BEBEBE")
    Red = Colour("red", "\033[91m", "FF0000")
    Green = Colour("green", "\033[92m", "00FF00")
    Yellow = Colour("yellow", "\033[93m", "FFFF00")
    Blue = Colour("blue", "\033[94m", "0000FF")
    NoColour = Colour("no-colour", "", "")
