from abc import ABC, abstractmethod
from typing import List

from zutun.etwas import Etwas, EtwasState


class EtwasStore(ABC):
    @abstractmethod
    def append(self, etwas: Etwas):
        pass

    @abstractmethod
    def remove(self, etwas: Etwas):
        pass

    @abstractmethod
    def save(self, etwasse: List[Etwas]):
        pass

    @abstractmethod
    def load(self, state: EtwasState) -> List[Etwas]:
        pass
