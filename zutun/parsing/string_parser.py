from datetime import datetime
from enum import Enum
from typing import List, Optional, Set, Union

from vinktech.error import VError
from vinktech.iterable import VListerator
from vinktech.string_accumulator import StringAccumulator
from zutun.dates import Dates
from zutun.etwas import Etwas, EtwasState


class ParsingMode(Enum):
    NORMAL = "normal"
    DUE = "DUE"
    TAG = "tag"


class StringParser:
    @staticmethod
    def parse(task: Union[str, List[str]]) -> Etwas:
        created: datetime = Dates.localnow()
        if isinstance(task, str):
            if "+" not in task and "@" not in task:
                created: datetime = Dates.localnow()
                due: datetime = Dates.next_due_datetime(created)
                return Etwas(task.strip(), EtwasState.Due, set(), due=due, created=created)
            task = task.split(" ")
        tokens: VListerator = VListerator(task)
        mode: ParsingMode = ParsingMode.NORMAL
        tags: Set[str] = set()
        due: Optional[datetime] = None
        quote: Optional[str] = None
        task_acc: StringAccumulator = StringAccumulator()
        tag_acc: StringAccumulator = StringAccumulator()
        due_acc: StringAccumulator = StringAccumulator()
        while tokens.has_next():
            token: str = tokens.next()
            if ParsingMode.NORMAL == mode:
                if token.startswith("+") and 1 < len(token):
                    token = token[1:]
                    if token.startswith("'") or token.startswith('"'):
                        mode = ParsingMode.TAG
                        quote = token[0]
                        token = token[1:]
                    tag_acc.accumulate(token)
                elif token.startswith("@"):
                    if due is not None:
                        raise VError("More than 1 due date found in task", task=task)
                    token = token[1:]
                    if token.startswith("'") or token.startswith('"'):
                        mode = ParsingMode.DUE
                        quote = token[0]
                        token = token[1:]
                    due_acc.accumulate(token)
                else:
                    task_acc.accumulate(token)
            elif ParsingMode.TAG == mode:
                if token.endswith(quote):
                    mode = ParsingMode.NORMAL
                    quote = None
                    token = token[:-1]
                tag_acc.accumulate(token)
            elif ParsingMode.DUE == mode:
                if token.endswith(quote):
                    mode = ParsingMode.NORMAL
                    quote = None
                    token = token[:-1]
                due_acc.accumulate(token)
            if tag_acc.has_data() and quote is None:
                tags.add(tag_acc.get())
            if due_acc.has_data() and quote is None:
                due = Dates.strpdt(due_acc.get())
        if due is None:
            due = Dates.next_due_datetime(created)
        else:
            due = Dates.ensure_due_datetime(due)
        return Etwas(task_acc.get().strip(), EtwasState.Due, tags, due=due, created=created)


if __name__ == "__main__":
    print(
        StringParser().parse(
            "talk to bob about the project #bobswork #theproject @2020-07-26T17:59"
        )
    )
