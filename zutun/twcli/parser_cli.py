import enum
from datetime import datetime
from typing import List
from typing import Optional, Set, Union

from tasklib import Task, TaskWarrior

from vinktech.error import VError
from vinktech.iterable import VListerator
from vinktech.string_accumulator import StringAccumulator
from zutun.dates import Dates
from zutun.etwas import Etwas, EtwasState


class ParsingMode(enum.Enum):
    NORMAL = "normal"
    DUE = "DUE"
    TAG = "tag"


class ZuTunCliParser:
    def __init__(self, tw: TaskWarrior):
        self.tw: TaskWarrior = tw

    def parse(self, task: Union[str, List[str]]) -> Task:
        created: datetime = Dates.localnow()
        if isinstance(task, str):
            if "+" not in task and "@" not in task:
                created: datetime = Dates.localnow()
                due: datetime = Dates.next_due_datetime(created)
                return Task(self.tw, description=task.strip(), due=due)
            task = task.split(" ")
        tokens: VListerator = VListerator(task)
        mode: ParsingMode = ParsingMode.NORMAL
        tags: Set[str] = set()
        due: Optional[datetime] = None
        quote: Optional[str] = None
        task_acc: StringAccumulator = StringAccumulator()
        tag_acc: StringAccumulator = StringAccumulator()
        due_acc: StringAccumulator = StringAccumulator()
        while tokens.has_next():
            token: str = tokens.next()
            if ParsingMode.NORMAL == mode:
                if token.startswith("+") and 1 < len(token):
                    token = token[1:]
                    if token.startswith("'") or token.startswith('"'):
                        mode = ParsingMode.TAG
                        quote = token[0]
                        token = token[1:]
                    tag_acc.accumulate(token)
                elif token.startswith("@"):
                    if due is not None:
                        raise VError("More than 1 due date found in task", task=task)
                    token = token[1:]
                    if token.startswith("'") or token.startswith('"'):
                        mode = ParsingMode.DUE
                        quote = token[0]
                        token = token[1:]
                    due_acc.accumulate(token)
                else:
                    task_acc.accumulate(token)
            elif ParsingMode.TAG == mode:
                if token.endswith(quote):
                    mode = ParsingMode.NORMAL
                    quote = None
                    token = token[:-1]
                tag_acc.accumulate(token)
            elif ParsingMode.DUE == mode:
                if token.endswith(quote):
                    mode = ParsingMode.NORMAL
                    quote = None
                    token = token[:-1]
                due_acc.accumulate(token)
            if tag_acc.has_data() and quote is None:
                tags.add(tag_acc.get())
            if due_acc.has_data() and quote is None:
                due = Dates.strpdt(due_acc.get())
        if due is None:
            due = Dates.next_due_datetime(created)
        else:
            due = Dates.ensure_due_datetime(due)
        return Task(self.tw, description=task_acc.get().strip(), due=due, tags=tags)
