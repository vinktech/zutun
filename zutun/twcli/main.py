import enum
import sys
from typing import Any, Dict, List

from vinktech.error import VError
from vinktech.iterable import VListerator
from zutun import VERSION
from zutun.dates import Dates
from zutun.twcli.hooks.manager import HookManager
from zutun.twcli.zutun_cli import ZuTunCli


class Command(enum.Enum):
    Add = {"add", "+"}
    List = {"list"}
    Complete = {"complete", "!"}
    Delete = {"delete", "-"}
    Yesterday = {"--yesterday", "-y"}
    Help = {"--help", "-h"}
    Version = {"--version", "-v"}

    @staticmethod
    def parse(maybe_command: str) -> "Command":
        for cmd in Command:
            if maybe_command in cmd.value:
                return cmd
        return Command.Add


def extract_id(arg: str) -> Dict[str, Any]:
    try:
        id_: int = int(arg)
        return {"id": id_}
    except:
        if ":" in arg:
            comps: List[str] = arg.split(":")
            if "id" == comps[0]:
                return {"id": int(comps[1])}
            elif "uuid" == comps[0]:
                return {"uuid": comps[1]}
            else:
                raise VError(f"Unknown task identifier {arg}")
        return {"uuid": arg}


def main():
    args: List[str] = sys.argv[1:]
    command: Command = Command.parse(args[0]) if 0 < len(args) else Command.List
    if 1 < len(args) and args[0] in command.value:
        args = args[1:]
    zutun: ZuTunCli = ZuTunCli()
    HookManager().run("~/.task/hooks")
    list_args: Dict[str, Any] = {}
    if Command.Yesterday == command:
        list_args["modifier"] = "completed"
        list_args["completed"] = Dates.strpdt("yesterday")
        command = Command.List

    if Command.Help == command:
        print(
            f"""zutun v. {VERSION}
  * add|+
    - any plain text is considered the body of the task
    - due date is prefixed with @, i.e: @today, @'in 3 days' @2020-04-13 @"2020-07-16 12:34"
      there can be only 1 due date
    - tags are prefixed with + (thanks bash), i.e. +project +'for mum' +"this is also a tag"
      can be specified multiple times
    - --completed at the start of a task will automatically complete it
  * list
    - empty list will show a default view of currently active tasks
    - use show:{{uuid,level,due,created,done,tags,comment}} to enable other elements of a task
      one per element
    - use age:<number> to filter tasks younger than <number>
      only one age filter allowed
    - use level:{{future,new,normal,a-bit-tardy,important,urgent}} to only show task at or above the given level
      only one level filter allowed
    - use tag:<tag> to only show tasks with the specified tags
      can be specified multiple times
      no current support for multi word tags
    - use completed:<date> to show task completed on the given date
      only one allowed
      single word only (i.e. yesterday or yyyy-MM-dd)
  * complete|! <id> [comment]
    - <id> must be the id specified next to the task when listed
    - [comment] is optional; any text after the id is considered part of the comment
  * delete|- <id> [comment]
    - <id> must be the id specified next to the task when listed
    - [comment] is optional; any text after the id is considered part of the comment
  * --yesterday or -y is an alias for zutun list completed completed:yesterday
  * --help or -h will display this help
  * --version or -v will display the version
  * if no arguments are provided, zutun will behave as if it has an plain `list` operation
  * if any text other than [add|+|list|complete|!|delete|-|--help|-h] is provided, zutun will assume it's an `add` operation
        """
        )
        return
    elif Command.Version == command:
        print(f"zutun v. {VERSION}")
        return
    elif Command.Add == command:
        already_complete: bool = False
        if 0 < len(args):
            if "--completed" == args[0]:
                already_complete = True
                args = args[1:]
            if 0 < len(args):
                zutun.add(args, already_complete)
    elif Command.Complete == command or Command.Delete == command:
        if 0 == len(args):
            raise VError(f"For {command.value}, at least 1 argument is required")
        kwargs: Dict[str, Any] = extract_id(args[0])
        comment: str = None if 1 == len(args) else " ".join(args[1:])
        if Command.Complete == command:
            zutun.complete(kwargs, comment)
        else:
            zutun.delete(kwargs, comment)
    else:
        modifier: List[str] = []
        vargs: VListerator = VListerator(args)
        while vargs.has_next():
            arg: str = vargs.next()
            if arg in {"all", "completed", "deleted"} and "modifier" not in list_args:
                modifier.append(arg)
            elif arg.startswith("show:"):
                list_args[arg.split(":")[1]] = True
            elif arg.startswith("age:"):
                if "age" in list_args:
                    raise VError(f"Age already specified as {list_args['age']}, but found {arg}")
                list_args["age"] = int(arg.split(":")[1])
            elif arg.startswith("level:"):
                if "levelf" in list_args:
                    raise VError(
                        f"Level already specified as {list_args['levelf']}, but found {arg}"
                    )
                list_args["levelf"] = arg.split(":")[1]
            elif arg.startswith("tag:"):
                if "tagsf" not in list_args:
                    list_args["tagsf"] = []
                tag: str = arg.split(":")[1]
                if not tag.startswith("+") and not tag.startswith("-"):
                    tag = f"+{tag}"
                list_args["tagsf"].append(tag)
            elif arg.startswith("completed:"):
                if "completed" in list_args:
                    raise VError(
                        f"Completed already specified as {list_args['completed']}, but "
                        "found {arg}"
                    )
                list_args["modifier"] = "completed"
                modifier.clear()
                list_args["completed"] = Dates.strpdt(arg.split(":")[1])

        if 1 < len(modifier):
            raise VError(f"Multiple list modifiers found: {modifier}")
        elif 1 == len(modifier):
            list_args["modifier"] = modifier[0]
    zutun.list(list_args)


if __name__ == "__main__":
    main()
