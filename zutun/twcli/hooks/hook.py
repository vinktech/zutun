import os
import subprocess
from abc import ABC


class Hook(ABC):
    def __init__(self, name: str, body: str):
        self.name: str = name
        self.body: str = body

    def apply(self, hooks_dir: str):
        if hooks_dir.endswith("/"):
            file_path: str = f"{hooks_dir}{self.name}"
        else:
            file_path: str = f"{hooks_dir}/{self.name}"
        if not os.path.isfile(file_path):
            with open(file_path, "w") as of:
                of.write(self.body)
            subprocess.call(f"chmod u+x {file_path}".split())
