#!/usr/bin/env python3

import datetime
import os
import subprocess
from typing import List


def quiet(cmd: str) -> int:
    return subprocess.call(cmd.split(), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def run(cmd: str) -> List[str]:
    output: str = subprocess.run(
        cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.DEVNULL
    ).stdout.decode().strip()
    if not output:
        return []
    return list(output.split("\\n"))


def ensure_dir():
    task_dir: str = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
    os.chdir(task_dir)


def is_git() -> bool:
    return 0 == quiet("git status")


def has_changes() -> bool:
    return 0 != quiet("git diff --exit-code") or 0 != len(
        [c for c in run("git status --porcelain") if c.startswith("A ")]
    )


def has_untracked() -> bool:
    return 0 != len(run("git ls-files . --exclude-standard --others"))


def commit(message: str):
    subprocess.call("git commit -a -m".split() + [f'"{message}"'])


def num_commits() -> int:
    return len(run("git --no-pager log @{u}..@ --oneline"))


def has_commits():
    return 0 != num_commits()


def main():
    ensure_dir()
    if is_git():
        changes: bool = has_changes()
        if has_untracked():
            quiet("git add -A")
            changes = True
        if changes:
            when: str = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f %z")
            commit(f"Changes as of {when}")
        if has_commits():
            push_at: int = int(os.getenv("ZUTUN_PUSH_AT", "1"))
            if 0 == push_at:
                print(
                    "Changes detected, but automatic pushing is disabled (ZUTUN_PUSH_AT==0). "
                    "Please push changes manually"
                )
            elif push_at >= num_commits():
                quiet("git pull --rebase")
                quiet("git push")
        else:
            quiet("git pull --rebase")


if __name__ == "__main__":
    main()
