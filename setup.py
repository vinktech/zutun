import setuptools


version: str = "0.0.10"

setuptools.setup(
    name="zutun",
    version=version,
    author="Mike Vink",
    author_email="code@vink.tech",
    description="Yet another todo manager, with my own twist",
    url="",
    packages=setuptools.find_packages(
        exclude=("test", "test.*", "tests", "tests.*", "vinktest", "vinktest.*")
    ),
    entry_points={"console_scripts": ["zutun = zutun.twcli.main:main"]},
)
