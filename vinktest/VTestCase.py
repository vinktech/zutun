import datetime
import shutil
import tempfile
import unittest
from typing import List

import pytest


class VTestCase(unittest.TestCase):
    # noinspection PyPep8Naming
    def __init__(self, methodName: str = "runTest"):
        super().__init__(methodName)
        self.temps: List[str] = []

    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog):
        self.caplog = caplog

    def tearDown(self) -> None:
        for temp in self.temps:
            shutil.rmtree(temp)

    def tmpdir(self, suffix: str = None, prefix: str = None, dir_: str = None) -> str:
        name: str = tempfile.mkdtemp(suffix, prefix, dir_)
        self.temps.append(name)
        return name

    def tmpfile(
        self,
        name: str,
        create: bool = False,
        suffix: str = None,
        prefix: str = None,
        dir_: str = None,
    ) -> str:
        dirname: str = tempfile.mkdtemp(suffix, prefix, dir_)
        self.temps.append(dirname)
        if create:
            raise ValueError("Not Implemented yet")
        return f"{dirname}/{name}"

    def assertDateTimeEqual(
        self,
        first: datetime,
        second: datetime,
        error: datetime.timedelta = datetime.timedelta(seconds=5),
    ):
        self.assertTrue(first - second < error)
